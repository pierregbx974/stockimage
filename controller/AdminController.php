<?php
require_once 'model/admin.php';

/**
 * 
 */
class AdminController
{
	private $admin;
	
	function __construct(){

		$this->admin = new admin();
	}

	public function afficheAllClient(){
		$data['admin'] = $this->admin->getAllClient();
		include 'view/admin/afficheAllClient.php';
		die;
	}

	public function ShowDossierByID(){
		$data['dossier'] = $this->admin->getAllDossierByIDcli($_GET['id_cli']);
		include 'view/admin/afficheDossier.php';
	}

	public function AfficheClient() {
    	$admin = $this->admin->getClient($_GET['id_cli']);
        if (!$admin) {
            die('Page Not Found 404');    
        }
    	include 'view/admin/afficheClient.php';
    }
}
?>