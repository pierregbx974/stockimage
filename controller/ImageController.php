<?php
require_once "model/image.php";

/**
 * 
 */
class ImageController{

	private $image;

	function __construct(){
		$this->image = new image();
	}

	function index(){
        $id_dossier = $_GET['id_dossier'];
        $data['images'] = $this->image->getImages($id_dossier);
		include "view/image/index.php";
	}

    public function GetImageId(){
        $id_image = $_GET['id_image'];
            $image = $this->image->GetImageId($id_image);
        include 'view/image/AfficheImage.php';
    }

	public function CreeImage(){
		$id_dossier = $_GET['id_dossier'];

		if (isset($_POST['submit'])) {
    $content_dir = 'upload'; // dossier où sera déplacé le fichier
 
    $tmp_file = $_FILES['fic']['tmp_name'];
 
    if( !is_uploaded_file($tmp_file) )
    {
        exit('<div class="alert alert-danger" role="alert">
                        Le fichier est introuvable !
                      </div>');
    }
 
    // on vérifie maintenant l'extension
    $type_file = $_FILES['fic']['type'];
 
    if(!strstr($type_file, 'png') && !strstr($type_file, 'jpg') && !strstr($type_file, 'jpeg') && !strstr($type_file, 'bmp') && !strstr($type_file, 'gif') )
    {
        exit('<div class="alert alert-warning" role="alert">
                        Le fichier n\'est pas une image !
                      </div>');
    }
 
    // on copie le fichier dans le dossier de destination
    $name_file = $_FILES['fic']['name'];
 
    if( !move_uploaded_file($tmp_file, "$content_dir/$name_file") )
    {
        exit("Impossible de copier le fichier dans $content_dir");
    }

    $Url_image="$content_dir/$name_file";
 
    echo '<div class="alert alert-success" role="alert">
                        Le ficheir a bien été uploader!
                      </div>';
    // et tu insères en base de données quelque chose du genre :
    // $URL = $content_dir . $name_file;


   $image = $this->image->CreeImage($Url_image,$id_dossier);
}
include 'view/image/CreeImage.php';
	}
	
}

?>