
<?php

require_once 'model/client.php';

class ClientController {

	private $client;

	public function __construct() {
		$this->client = new client();
	}

        public function index($notification = '') {
        $data['notification'] = $notification;
        include 'view/index.php';
        die;
    }

    public function authentification()
    {
        if (isset($_POST['submit'])) {
            $identification=$this->client->authentification($_POST);
             
                if ($_POST['Password_cli']==$identification['Password_cli']) {

                    $_SESSION['id_client'] = $identification['id_client'];

                    $_SESSION['Droit'] = $identification['Droit'];

                    $_SESSION['Adresse_mail_cli'] = $_POST['Adresse_mail_cli'];

                        echo "<div class='alert alert-success' role='alert'>
                                Connexion réussie!
                                </div>";
        }
        else{
                echo "<div class='alert alert-danger' role='alert'>
                        Adresse mail ou mot de passe incorect, veuillez recommencer!
                        </div>";

    }
    $this->index(); // Redirection vers l'index

}

         include 'view/client/authentification.php';
}

    public function inscription() {
        $errors = array();

        if (isset($_POST['submit'])) {
             if (empty($_POST['Nom_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le Nom est obligatoire !
                      </div>';
            }
            if (empty($_POST['Prenom_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le Prénom est obligatoire !
                      </div>';
            }
            if (!filter_var($_POST['Adresse_mail_cli'], FILTER_VALIDATE_EMAIL) && !empty($_POST['Adresse_mail_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        L\'adresse mail est obligatoire !
                      </div>';
            }
            if (empty($_POST['Password_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le mot de passe est obligatoire !
                      </div>';
            }
            if (empty($_POST['Adresse_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        L\'adresse est obligatoire !
                      </div>';
            }
            if (empty($_POST['Code_postal_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le code postal est obligatoire !
                      </div>';
            }
            if (empty($errors)) {
                $add = $this->client->inscription($_POST);
                if ($add) {
                   
                    echo "<div class='alert alert-success' role='alert'>
                                Inscription réussie!
                                </div>";
                } 
                else {
                   
                   echo "<div class='alert alert-danger' role='alert'>
                        Une erreur s'est produite!
                        </div>";
                }
                $this->index(); // Redirection vers l'index
            }
        }
        include 'view/client/inscription.php';
    }

     public function AfficheCompte() {
        $client = $this->client->getClient($_SESSION['id_client']);

        if (isset($_POST['submit'])) {

            if (empty($_POST['Nom_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le nom est obligatoire !
                      </div>';
            }
            if (empty($_POST['Prenom_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le Prénom est obligatoire !
                      </div>';
            }
            if (!filter_var($_POST['Adresse_mail_cli'], FILTER_VALIDATE_EMAIL) && !empty($_POST['Adresse_mail_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        l\'adresse email est obligatoire !
                      </div>';
            }
            if (empty($_POST['Password_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le mot de passe est obligatoire !
                      </div>';
            }
            if (empty($_POST['Adresse_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        L\'adresse est obligatoire !
                      </div>';
            }
            if (empty($_POST['Code_postal_cli'])) {
                echo '<div class="alert alert-warning" role="alert">
                        Le code postal est obligatoire !
                      </div>';
            }

            if (empty($errors)) {
                 $edit=$this->client->ModifeCompte($_POST,$_SESSION['id_client']);
                    if (!empty($edit)) {
                    echo "<div class='alert alert-success' role='alert'>
                            Modification réussie!
                            </div>";
                } 
                else {
                    /*echo "probleme, ou Mot de passe non identiques";*/
                    echo "<div class='alert alert-danger' role='alert'>
                            Une erreur s'est produite, ou les deux mots de passe doivent être identiques!
                            </div>";
                }
                $this->index(); // Redirection vers l'index
            }
        }
        include 'view/client/Affichecompte.php';
    }

}