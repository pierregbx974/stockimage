<?php
require_once 'model/dossier.php';

/**
 * 
 */
class DossierController {
	private $dossier;

	public function __construct() {
		$this->dossier = new dossier();
	}

	/*public function index(){
        
            
         $data['dossier'] = $this->dossier->getDossierByid($id);
		include 'view/dossier/index.php';
	}*/

	public function index(){
        $id_cli= @$_GET['id_cli'];
        $data['dossier'] = $this->dossier->getDossierByid($id_cli); 
        include 'view/dossier/index.php';
    }

    public function CreeDossier(){
    	
    }

    public function AjoutDossier(){

        if (isset($_POST['submit'])) {
            
        //Récupération de l'id client
            $id_cli = $_GET['id_cli'];

            
        if (empty($_POST['Nom_dossier'])) {
                echo "<div class='alert alert-warning' role='alert'>
                        Veuillez saisir un nom de dossier!
                    </div>";
            }
            else{
               $AjoutDossier = $this->dossier->AjoutDossier($_POST,$id_cli);
            echo "<div class='alert alert-success' role='alert'>
                        Dossier bien crée!
                    </div>"; 
            }
    	$this->index(); // Redirection vers l'index
        exit();
    }
    include 'view/dossier/CreeDossier.php';
    }

    public function SupprDossier(){

        $suppr = $this->dossier->SupprDossier($_GET['id_dossier']);

        if ($suppr) {
            echo '<div class="alert alert-success" role="alert">
                        Le dossier est bien supprimer !
                      </div>';
        } 

        else {
            echo '<div class="alert alert-danger" role="alert">
                        Impossible de supprimer le dossier !
                      </div>';

        }

            $this->index(); // Redirection vers l'index
        }

    public function ModifDossier(){
    $id_dossier = $_GET['id_dossier'];
    
    $dossier = $this->dossier->getDossier($id_dossier);

    if(isset($_POST['submit'])){

       if (!isset($_POST['Nom_dossier'])) {
            echo '<div class="alert alert-warning" role="alert">
                        Le nom de dossier est obligatoire !
                      </div>';
        }

        else{

            $modifDossier= $this->dossier->ModifDossier($_POST,$id_dossier);

            if (isset($modifDossier)) {
                echo '<div class="alert alert-success" role="alert">
                        Le nom du dossier a bien été modifier !
                      </div>';
            }

            else{
                echo '<div class="alert alert-danger" role="alert">
                        Imposible de modifier le nom du dossier
                      </div>';
            }
            $this->index();
            exit();
        }
    }
      include 'view/dossier/modifDossier.php';
    }
}

?>