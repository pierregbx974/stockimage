
<?php include "templates/header.php";?>
<?php include "css/script.php";?>

<br>
<center>
<table>
	<thead>
		<tr>
			<th>Client n°</th>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Mot de passe</th>
			<th>Email</th>
			<th>Ville</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if ($data['admin']) {
			foreach ($data['admin'] as $k => $v) {
			?>
				<tr>
					<td><?php echo $k+1; ?></td>
					<td><?php echo $v['Nom_cli']; ?></td>
					<td><?php echo $v['Prenom_cli']; ?></td>
					<td><?php echo $v['Password_cli']; ?></td>
					<td><a href="mailto:<?php echo $v['Adresse_mail_cli']; ?>"><?php echo $v['Adresse_mail_cli']; ?></a></td>
					<td><a href="https://www.google.com/maps?q=<?php echo $v['Adresse_cli']." ".$v['Code_postal_cli']; ?>"><?php echo $v['Adresse_cli']." ".$v['Code_postal_cli']; ?></a></td>
					<td>
						<a href="?ctrl=admin&mth=AfficheClient&id_cli=<?php echo $v['id_client']; ?>">Lire |</a>
						<a href="?ctrl=employe&mth=edit&id=<?php echo $v['id_client']; ?>">Modifier |</a>
					</td>
				</tr>
			<?php
			}
		} else {
			?>
			<tr>
				<td colspan="6">Pas d'employé</td>
			</tr>
		<?php
		}
		?>
	</tbody>
</table>
</center><br><br>

<ul class='nav nav-pills nav-fill'>
  <li class='nav-item'>
    <a style='width:25%;margin-left:35%;margin-top: 2%;' class='nav-link active' href='?ctrl=Accueil&mth=index'>Retour au menu</a>
  </li>
</ul><br><br>

<?php include 'templates/footer.php'; ?>