<?php include 'templates/header.php'; ?>
<link rel="stylesheet" type="text/css" href="css/style.css">
 <div id="images1">
<img height="350" width="100%" src="templates/image/imagesbord.jpeg">
</div>
  <br>
  <div class="container marketing">
    <!-- Three columns of text below the carousel -->
    <div class="row">
      <div class="col-lg-4">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice"><i class="fas fa-thumbs-up" style="color: blue;font-size: 5.0em;margin-left: -10%;"></i></svg><br><br>
        <h2>Simple d'utlisation</h2>
        <p>Afin de découvire comment fonctionne notre site, il est grand temps pour vous de cliquez sur le lien ci-dessous, afin de pouvoir consulter notre documentation utilisateur, et de suivre les instructions énumérés sur la page suivante.</p>
        <p><a class="btn btn-primary" href="#" role="button"><i class="fas fa-book"></i> Documentation</a></p>
      </div><!-- /.col-lg-4 -->

      <div class="col-lg-4">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><i class="fas fa-cloud-download-alt" style="color: blue;font-size: 5.0em;margin-left: -10%;"></i></svg><br><br>
        <h2>Efficacité de sauvegarde</h2>
        <p>Il vous suffis juste de telecharger votre image, ensuite si vous souhaitez la récupérer, il vous suffit de vous connecter.<br>
    Ensuite d\'acceder a cette image vous pouvez le re télécharger pour la stocker sur votre ordinateur.</p>

      </div><!-- /.col-lg-4 -->
      <div class="col-lg-4">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140"><i class="fas fa-images" style="color: blue;font-size: 5.0em;margin-left: -10%;"></i></svg><br><br>
        <h2>Un album rangé</h2>
        <p>Un site ordoné ! Vous pouvez crée plusieurs dossier contenant des images afin de mieux vous repérer dans ce que vous stocker, il est grand temps de mettre de l\'odre dans vos photos.</p>
      </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->

        <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7">
        <h2><span class="text-muted">La photographie un art ancestral.</span></h2>
        <p class="lead">La photographie est apprécié de tous, c\'est un art qui peut être pratiquer sans avoir d\'énormes talents artistiques.<br>C\'est ce qui la rend universelle et simple à utiliser.<br>La première photographie de couleur date de 1861, et elle à été réalisée par Thomas stuffon.</p>
      </div>
      <div class="col-md-5">
        <img id="img1" src="templates/image/premierePhoto.jpg" width="500" height="350">
      </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
      <div class="col-md-7 order-md-2" id="text2">
        <h2><span class="text-muted">Les améliorations de qualitée.</span></h2>
        <p class="lead">Aujourd\'hui la photographie, a subit une évolution fulgurante avec l\'amélioration des qualités photos.<br>
        Ce qui pousse les personnes a prendre des photos de plus en plus audicieuses et magnifique. Nous, nous somme là afin que vous puissiez ne jamais perdre vos plus beaux clichez! Alors qu\'attendez vous pour nous utilisé ?</p>
      </div>
      <div class="col-md-5">
        <img id="img2" src="templates/image/secondePhoto.jpg" width="450" height="350">
      </div>
    </div>

<?php include 'templates/footer.php'; ?>