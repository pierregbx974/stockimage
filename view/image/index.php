<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
<?php include "templates/header.php";?>
<body>

<center><h1>Images</h1></center>
		
<?php
	if (isset($data['images'])) {
		foreach ($data['images'] as $k => $v) {    
	?>
    <div class="card mb-4 shadow-sm" style="width: 25%;display: inline-block;margin-left: 6.5%;">
          <img src="<?php echo $v['Url_image'];?>" class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Mes images</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"></text>
          <div class="card-body">
                  <div class="d-flex justify-content-between align-items-center">
                    <div class="btn-group">
                        <a href="?ctrl=image&mth=GetImageId&id_image=<?php echo $v['id_image']; ?>" type="submit" id="submit" class="btn btn-sm btn-outline-secondary">View</a>
                        <a href="?ctrl=dossier&mth=SupprImage&id_image=<?php echo $v['id_image']; ?>" name="submit" type="submit" id="submit" class="btn btn-sm btn-outline-secondary">Delete</a>

                    </div>
                <small class="text-muted">9 mins</small>
                  </div>
            </div>
         </div>		
<?php
	}
		} else {
?>
  <center>
			<tr>
				<td colspan="6">Pas d'images</td>
			</tr>

		<?php
		}
    $id_dossier= $_GET['id_dossier'];
		?>
	<center><p><a class="btn btn-primary" href="?ctrl=image&mth=CreeImage&id_dossier=<?php echo $id_dossier;?>" role="button"><i class="fas fa-file-image"></i> Ajouter une image</a></p>
  </center>

</body>
<?php include "templates/footer.php";?>

