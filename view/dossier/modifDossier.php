

<?php include "templates/header.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Nom de mon dossier</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
<link href="#" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom styles for this template -->
  </head>
  <center>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img style="width:12%; height: auto;" class="mb-4" src="templates/image/logo1.png" alt="">
    <h2>Modifier le nom de mon dossier</h2>
    <p class="lead">Tappez le nouveau nom de votre dossier</p>
  </div>
    <div class="col-md-8 order-md-1"> 

      <form action="?ctrl=dossier&mth=ModifDossier&id_cli=<?php echo $dossier['id_cli'] ?>&id_dossier=<?php echo $dossier['id_dossier'] ?>" method="post">

        <div class="row">
          <div class="col-md-6 mb-3" style="margin-left: 20%">
            <label for="Nom_dossier">Nom du dossier</label>
            <input type="text" class="form-control" name="Nom_dossier" id="Nom_dossier" value="<?php echo $dossier['Nom_dossier']; ?>">
          </div>

        <input class="btn btn-primary btn-lg btn-block" id="submit" name="submit" type="submit" value="Modifier">
      </form>
    </div>
  </div>
</div>
</center>
</html>

<?php include "templates/footer.php";?>