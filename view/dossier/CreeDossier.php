<?php include "templates/header.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Crée un dossier</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
<link href="#" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom styles for this template -->
  </head>
  <center>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img style="width:12%; height: auto;" class="mb-4" src="templates/image/logo1.png" alt="">
    <h2>Création d'album</h2>

  </div>
    <div class="col-md-8 order-md-1"> 
      <form action="?ctrl=dossier&mth=AjoutDossier&id_cli=<?php echo $_SESSION["id_client"];?>" method="post">

        <div class="row" style="margin-left: 25%;">
          <div class="col-md-6 mb-3">
            <label for="Nom_dossier">Nom du dossier</label>
            <input type="text" class="form-control" name="Nom_dossier" id="Nom_dossier">
          </div>
        </div>

        <button  class="btn btn-primary btn-lg btn-block" name="submit" id="submit" type="submit" style="margin-top: 5%;">Crée l'album</button>
      </form>
  </div>
</div>
</center>
</html>

<?php include "templates/footer.php";?>