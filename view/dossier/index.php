  <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
<?php include "templates/header.php";?>
<body>

<center><h1>Dossier</h1></center>
		
<?php
	if ($data['dossier']) {
		foreach ($data['dossier'] as $k => $v) {
	?>
        <div class="card mb-4 shadow-sm" style="width: 25%;display: inline-block;margin-left: 6.5%;">
        	<svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title>Placeholder</title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"></text></svg>
         	<div class="card-body">
              	<center><p class="card-text"><?php echo $v['Nom_dossier']; ?></p></center>
              		<div class="d-flex justify-content-between align-items-center">
                		<div class="btn-group">
                        <a href="?ctrl=image&mth=index&id_dossier=<?php echo $v['id_dossier']; ?>" type="submit" id="submit" class="btn btn-sm btn-outline-secondary">View</a>
                        <a href="?ctrl=dossier&mth=ModifDossier&id_dossier=<?php echo $v['id_dossier']; ?>" type="button" class="btn btn-sm btn-outline-secondary">Edit</a>
                        <a href="?ctrl=dossier&mth=SupprDossier&id_cli=<?php echo $_SESSION['id_client'] ?>&id_dossier=<?php echo $v['id_dossier']; ?>" name="submit
              " type="submit" id="submit" class="btn btn-sm btn-outline-secondary">Delete</a>

                		</div>
                <small class="text-muted">9 mins</small>
              		</div>
            </div>
         </div>
<?php
	}
		} else {
?>
  <center>
			<tr>
				<td colspan="6">Pas d'album</td>
			</tr>
  </div>
      </div>
		<?php
		}
		?>
	<center><p><a class="btn btn-primary" href="?ctrl=dossier&mth=AjoutDossier&id=<?php echo $_SESSION["id_client"];?>" role="button"><i class="fas fa-file-image"></i> Crée un dossier</a></p>
  </center>

</body>
<?php include "templates/footer.php";?>