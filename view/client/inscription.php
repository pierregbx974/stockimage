<?php include "templates/header.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Inscription</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Bootstrap core CSS -->
<link href="#" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Custom styles for this template -->
  </head>
  <center>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img style="width:12%; height: auto;" class="mb-4" src="templates/image/logo1.png" alt="">
    <h2>Formulaire d'inscription</h2>
    <p class="lead">Pour utiliser notre site, il faut vous inscrire et ensuite suivre la documentation pour l'utilisation.</p>
  </div>
    <div class="col-md-8 order-md-1"> 
      <form action="?ctrl=client&mth=inscription" method="post">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="Nom_cli">Nom</label>
            <input type="text" class="form-control" name="Nom_cli" id="Nom_cli">
          </div>

          <div class="col-md-6 mb-3">
            <label for="Prenom_cli">Prénom</label>
            <input type="text" class="form-control" name="Prenom_cli" id="Prenom_cli">
          </div>
        </div>

        <div class="mb-3">
          <label for="Adresse_mail_cli">Email</label>
          <input type="email" class="form-control" name="Adresse_mail_cli" id="Adresse_mail_cli" placeholder="you@example.com">
        </div>

        <div class="mb-3">
          <label for="Password_cli">Mot de passe</label>
          <input type="password" class="form-control" name="Password_cli" id="Password_cli" placeholder="123azerty">
        </div>

        <!--<div class="mb-3">
          <label for="Password_cli">Mot de passe</label>
          <input type="password" class="form-control" name="Password_cli" id="Password_cli" placeholder="retaper votre mot de passe">
        </div>-->

        <div class="mb-3">
          <label for="Adresse_cli">Adresse</label>
          <input type="text" class="form-control" name="Adresse_cli" id="Adresse_cli" placeholder="1 rue de ...">
        </div>

        <div class="mb-3">
          <label for="Code_postal_cli">Code postal</label>
          <input type="number" class="form-control" name="Code_postal_cli" id="Code_postal_cli" placeholder="97429">  
        </div>

        <button class="btn btn-primary btn-lg btn-block" name="submit" type="submit">Inscription</button>
      </form>
    </div>
  </div>
</div>
</center>
</html>

<?php include "templates/footer.php";?>