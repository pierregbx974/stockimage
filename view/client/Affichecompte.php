

<?php include "templates/header.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Modifier mes informations</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/checkout/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


<link href="#" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  
  <style type="text/css">
 
.AffichePass
 {
  font-size: 15px;
  position: absolute;
  cursor: pointer;
  margin-left: 45%;
  margin-top: -4%;
}

.AffichePass:hover{
    color: blue;
}
  </style>

  </head>
  <center>
  <body class="bg-light">
    <div class="container">
  <div class="py-5 text-center">
    <img style="width:12%; height: auto;" class="mb-4" src="templates/image/logo1.png" alt="">
    <h2>Formulaire d'inscription</h2>
    <p class="lead">Pour utiliser notre site, il faut vous inscrire et ensuite suivre la documentation pour l'utilisation.</p>
  </div>
    <div class="col-md-8 order-md-1"> 
      <form action="?ctrl=client&mth=AfficheCompte&id=<?php echo $_SESSION['id_client'] ?>" method="post">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="Nom_cli">Nom</label>
            <input type="text" class="form-control" name="Nom_cli" id="Nom_cli" value="<?php echo $client['Nom_cli']; ?>">
          </div>

          <div class="col-md-6 mb-3">
            <label for="Prenom_cli">Prénom</label>
            <input type="text" class="form-control" name="Prenom_cli" id="Prenom_cli" value="<?php echo $client['Prenom_cli']; ?>">
          </div>
        </div>

        <div class="mb-3">
          <label for="Adresse_mail_cli">Email</label>
          <input type="email" class="form-control" name="Adresse_mail_cli" id="Adresse_mail_cli" value="<?php echo $client['Adresse_mail_cli']; ?>">
        </div>

        <div class="mb-3">
          <label for="Password_cli">Mot de passe</label> 
          <input type="password" class="form-control" name="Password_cli" id="Password_cli" value="<?php echo $client['Password_cli']; ?>"><span class="AffichePass"><i class="fas fa-eye-slash"></i></span>
        </div>

        <div class="mb-3">
          <label for="PasswordVerif">Vérification mot de passe</label> 
          <input type="password" class="form-control" name="PasswordVerif" id="PasswordVerif" value="<?php echo $client['Password_cli'] ?>"><span class="AffichePass"><i class="fas fa-eye-slash"></i></span>
        </div>

        <div class="mb-3">
          <label for="Adresse_cli">Adresse</label>
          <input type="text" class="form-control" name="Adresse_cli" id="Adresse_cli" value="<?php echo $client['Adresse_cli']; ?>">
        </div>

        <div class="mb-3">
          <label for="Code_postal_cli">Code postal</label>
          <input type="number" class="form-control" name="Code_postal_cli" id="Code_postal_cli" value="<?php echo $client['Code_postal_cli']; ?>">  
        </div>

        <button class="btn btn-primary btn-lg btn-block" name="submit" type="submit">Modifier</button>
      </form>
    </div>
  </div>
</div>
</center>
</html>

<script type="text/javascript">
$(document).ready(function(){
 
      $('.AffichePass').click(function() {
        if($(this).prev('input').prop('type') == 'password') {
          //Si c'est un input type password
          $(this).prev('input').prop('type','text');
        } else {
          //Sinon
          $(this).prev('input').prop('type','password');
        }
      });
 
    });
</script>

<?php include "templates/footer.php";?>