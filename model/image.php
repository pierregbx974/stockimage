<?php

require_once 'connexionDB.php';

class image extends ConnexionDB  {

	public function getImages($id_doss){
		return $this->cnx->query("SELECT * FROM image,dossier WHERE dossier.id_dossier=image.id_doss AND id_doss=$id_doss");
	} 

	public function CreeImage($Url_image,$id_dossier){
	$sql =$this->cnx->prepare("INSERT INTO image (Url_image,id_dossier) VALUES (?,?)");
	$sql->execute(array($Url_image,$id_dossier));
	return $sql->rowCount();
	}

	public function GetImageId($id_image){
		$sql=$this->cnx->prepare("SELECT * FROM image WHERE id_image=$id_image");
		$sql->execute(array($id_image));
		return $sql->fetch();
	}
}

?>