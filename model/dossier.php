<?php

require_once 'connexionDB.php';

class dossier extends ConnexionDB  {

	public function getDossier($id_dossier){
		$sql = $this->cnx->prepare("SELECT * FROM dossier WHERE id_dossier=$id_dossier");
		$sql->execute(array($id_dossier));
		return $sql->fetch();
	}

	public function getDossierID($id_dossier){
		$sql = $this->cnx->prepare("SELECT Nom_dossier FROM dossier WHERE id_dossier = ?");
		$sql = execute(array($dossier['id_dossier']));
		return $sql-> rowCount();
	}

	public function AjoutDossier($dossier,$id)
	{
		$sql = $this->cnx->prepare("INSERT INTO dossier (Nom_dossier,id_cli)
        VALUES (?,$id)");
		$sql->execute( array($dossier['Nom_dossier']) );
		return $sql->rowCount();
	}

	public function ModifDossier($dossier,$id_dossier){
		$sql = $this->cnx->prepare("UPDATE dossier SET Nom_dossier=? WHERE id_dossier=$id_dossier");
		$sql->execute(array($dossier['Nom_dossier']));
		return $sql->rowCount();
	}

	public function getDossierByid($id){
		return $this->cnx->query("SELECT id_dossier,Nom_dossier FROM dossier WHERE id_cli=$id");
	}

	public function SupprDossier($id_dossier){
		$sql = $this->cnx->prepare("DELETE FROM dossier WHERE id_dossier = $id_dossier");
		$sql->execute( array($id_dossier) );
		return $sql->rowCount();
	}
}