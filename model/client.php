<?php

require_once 'connexionDB.php';

class client extends ConnexionDB  {

	public function inscription($client)
	{
		/*$mdp = password_hash($_POST['Password_cli'], PASSWORD_DEFAULT);*/
		$sql = $this->cnx->prepare("INSERT INTO client (Nom_cli,Prenom_cli,Adresse_mail_cli,Password_cli,Adresse_cli,Code_postal_cli) 
        VALUES (?,?,?,?,?,?)");
		$sql->execute( array($client['Nom_cli'],$client['Prenom_cli'],$client['Adresse_mail_cli'],$client['Password_cli'],$client['Adresse_cli'],$client['Code_postal_cli']) );
		return $sql->rowCount();
	}
	public function authentification($client){
		$sql = $this->cnx->prepare("SELECT id_client, Password_cli, Prenom_cli, Droit FROM client WHERE Adresse_mail_cli = ?");
		$sql->execute(array(@$client['Adresse_mail_cli']));
		return $sql->fetch();
	}

		public function getClient($id) {
		$sql = $this->cnx->prepare("SELECT * FROM client WHERE id_client=$id");
		$sql->execute( array($id));
		return $sql->fetch();
	}

	public function ModifeCompte($client,$id){
		$sql= $this->cnx->prepare("UPDATE client SET Nom_cli=?,Prenom_cli=?,Adresse_mail_cli=?,Password_cli=?,Adresse_cli=?,Code_postal_cli=? WHERE id_client=?");
		$sql->execute( array($client['Nom_cli'],$client['Prenom_cli'],$client['Adresse_mail_cli'],$client['Password_cli'],$client['Adresse_cli'],$client['Code_postal_cli'],$id) );
	}
}