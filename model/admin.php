<?php

require_once 'connexionDB.php';

class admin extends ConnexionDB  {
	
	public function getAllClient(){
		return $this->cnx->query("SELECT * FROM client")->fetchAll();
	}

	public function getClient($id){
		$sql = $this->cnx->prepare("SELECT count(Nom_dossier),Nom_cli,Prenom_cli,Password_cli,Adresse_mail_cli,Adresse_cli,Code_postal_cli,id_client FROM client,dossier WHERE id_cli=? AND client.id_client=dossier.id_cli");
		$sql->execute(array($id));
		return $sql->fetch();
	}

	public function getAllDossierByIDcli ($id){
		$sql = $this->cnx->prepare("SELECT Nom_dossier FROM dossier WHERE id_cli=? ");
		$sql->execute(array($id));
		return $sql->fetch();
	}
}